//
//  AcademyFlow.swift
//  AcademyModule
//
//  Created by Engineer on 15/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation
import Core

public class AcademyFlow: Flow, AcademyDelegate {
    public var finish: (Flow) -> () = { _ in }
    public var services: Services
    public var navigation: UINavigationController?
    
    public required init(services: Services, navigationVC: UINavigationController?) {
        self.services = services
        self.navigation = navigationVC
    }
    
    public func start() {
        presentFeature1()
    }
    
    private func presentFeature1() {
        let vc = AcademyMainViewController(services: self.services)
        vc.delegate = self
        self.navigation?.pushViewController(vc, animated: true)
    }
    
    func wantsToGoBack(at vc: AcademyMainViewController) {
        vc.dismiss(animated: true, completion: nil)
        finish(self)
    }
}
