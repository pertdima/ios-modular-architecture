
//
//  AcademyHomeViewController.swift
//  AcademyModule
//
//  Created by Engineer on 15/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Commons
import Core
import Foundation

class AcademyMainViewController : CCBaseVC {
    weak var delegate: AcademyDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        self.view = UIView()
        self.view.addBackgroundColor(addColor: .white)
    }
    
    private func initVit() {
        
    }
    
    @objc func didClickClose() {
        delegate?.wantsToGoBack(at: self)
    }
    
    override func didMove(toParent parent: UIViewController?) {
        if parent == nil {
            self.delegate?.wantsToGoBack(at: self)
        }
    }
}
