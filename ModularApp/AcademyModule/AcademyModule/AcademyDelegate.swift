//
//  AcademyDelegate.swift
//  AcademyModule
//
//  Created by Engineer on 15/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

protocol AcademyDelegate: class {
    func wantsToGoBack(at vc: AcademyMainViewController)
}
