//
//  UIView.swift
//  Commons
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

public extension UIView{
    
    func roundedView(cornerRadius: CGFloat, bgColor: AppColor, isShadow: Bool){
        backgroundColor = bgColor.color
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.clear.cgColor
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        if isShadow {
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2.0)
            layer.shadowRadius = 5.5
            layer.shadowOpacity = 0.1
            layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.layer.cornerRadius).cgPath
        }
    }
    
    func setGradientBackground(topColor: AppColor, bottomColor:AppColor) {
        let colorTop =  topColor.color.cgColor
        let colorBottom = bottomColor.color.cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func addBackgroundColor(addColor: AppColor) {
        self.backgroundColor = addColor.color
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        
        var viewsDict = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDict["v\(index)"] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDict))
    }
    
    func addConstraintsFillEntireView(view: UIView) {
        addConstraintsWithFormat(format: "H:|[v0]|", views: view)
        addConstraintsWithFormat(format: "V:|[v0]|", views: view)
    }
    
    func consXCenter(toView: UIView) {
        self.centerXAnchor.constraint(equalTo: toView.centerXAnchor).isActive = true
    }
    
    func consXCenterWithConstant(toView: UIView, constant: CGFloat) {
        self.centerXAnchor.constraint(equalTo: toView.centerXAnchor, constant: constant).isActive = true
    }
    
    func consYCenter(toView: UIView) {
        self.centerYAnchor.constraint(equalTo: toView.centerYAnchor).isActive = true
    }
    
    func consYCenterWithConstant(toView: UIView, value: CGFloat) {
        self.centerYAnchor.constraint(equalTo: toView.centerYAnchor, constant: value).isActive = true
    }
    
    func consLeadingSuperView(view: UIView, value: CGFloat) {
        if #available(iOS 11.0, *) {
            self.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: value).isActive = true
        }else{
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: value).isActive = true
        }
    }
    
    func consTrailingSuperView(view: UIView, value: CGFloat) {
        if #available(iOS 11.0, *) {
            self.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -value).isActive = true
        }else{
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -value).isActive = true
        }
    }
    
    func consTopSuperView(view: UIView, value: CGFloat) {
        if #available(iOS 11.0, *) {
            self.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant:value).isActive = true
        }else{
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: value).isActive = true
        }
    }
    
    func consBottomSuperView(view: UIView, value: CGFloat) {
        if #available(iOS 11.0, *) {
            self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -value).isActive = true
        }else{
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -value).isActive = true
        }
    }
    
    func consHeightGreater(value: CGFloat){
        self.heightAnchor.constraint(greaterThanOrEqualToConstant: value)
    }
    
    func consHeightLess(value: CGFloat){
        self.heightAnchor.constraint(lessThanOrEqualToConstant: value)
    }
    
    func consLeftSuperView(view: UIView, value: CGFloat) {
        self.leftAnchor.constraint(equalTo: view.leftAnchor, constant: value).isActive = true
    }
    
    func consLeftToView(view: UIView, value: CGFloat) {
        self.leftAnchor.constraint(equalTo: view.rightAnchor, constant: value).isActive = true
    }
    
    func consRightSuperView(view: UIView, value: CGFloat) {
        self.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -value).isActive = true
    }
    
    func consRightToView(view: UIView, value: CGFloat) {
        self.rightAnchor.constraint(equalTo: view.leftAnchor, constant: -value).isActive = true
    }
    
    func consWidthView(value: CGFloat) {
        self.widthAnchor.constraint(equalToConstant: value).isActive = true
    }
    
    func consWidthToView(toView: UIView, multi: CGFloat) {
        self.widthAnchor.constraint(equalTo: toView.widthAnchor, multiplier: multi).isActive = true
    }
    
    func consHeightToView(toView: UIView, multi: CGFloat) {
        NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: toView, attribute: .height, multiplier: multi, constant: 0).isActive = true
    }
    
    func consLayoutHeightView(value: CGFloat) -> NSLayoutConstraint{
        return self.heightAnchor.constraint(equalToConstant: value)
    }
    
    func consHeightView(value: CGFloat) {
        self.heightAnchor.constraint(equalToConstant: value).isActive = true
    }
    
    func updateConsHeightView(view: NSLayoutConstraint, value: CGFloat) {
        view.constant = value
    }
    
    func consTopToView(toView: UIView, value: CGFloat) {
        self.topAnchor.constraint(equalTo: toView.bottomAnchor, constant: value).isActive = true
    }
    
    public func consBottomToView(toView: UIView, value: CGFloat) {
        self.bottomAnchor.constraint(equalTo: toView.topAnchor, constant: -value).isActive = true
    }
    
    func consPositionTopView(superView:UIView, bottom: CGFloat,left: CGFloat, right: CGFloat, height: CGFloat) {
        self.consBottomSuperView(view: superView, value: bottom)
        self.consTrailingSuperView(view: superView, value: right)
        self.consLeadingSuperView(view: superView, value: left)
        self.consHeightView(value: height)
    }
    
    func consSuperView(superView:UIView, top: CGFloat,left: CGFloat, right: CGFloat, bottom: CGFloat) {
        self.consTopSuperView(view: superView, value: top)
        self.consTrailingSuperView(view: superView, value: right)
        self.consLeadingSuperView(view: superView, value: left)
        self.consBottomSuperView(view: superView, value: bottom)
    }
    
    func consPositionBottomView(superView:UIView, top: CGFloat,left: CGFloat, right: CGFloat, height: CGFloat) {
        self.consTopSuperView(view: superView, value: top)
        self.consTrailingSuperView(view: superView, value: right)
        self.consLeadingSuperView(view: superView, value: left)
        self.consHeightView(value: height)
    }
    
    func consPositionBottomView(superView:UIView, toView: UIView, top: CGFloat, left: CGFloat, right: CGFloat, height: CGFloat) {
        self.consTopToView(toView: toView, value: top)
        self.consHeightView(value: height)
        self.consLeftSuperView(view: superView, value: left)
        self.consRightSuperView(view: superView, value: right)
    }
    func consPositionBottomView(superView:UIView, toView: UIView, top: CGFloat, left: CGFloat, right: CGFloat, multi: CGFloat) {
        self.consTopToView(toView: toView, value: top)
        self.consLeftSuperView(view: superView, value: left)
        self.consRightSuperView(view: superView, value: right)
        self.consHeightToView(toView: superView, multi: multi)
    }
}
