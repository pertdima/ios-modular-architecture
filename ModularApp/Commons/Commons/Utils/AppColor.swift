//
//  AppColor.swift
//  Commons
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import UIKit

public enum AppColor {
    case colorPrimary, white, black
    
    public var color: UIColor {
        switch self {
        case .colorPrimary:
             return UIColor(red: 0.41, green: 0.19, blue: 0.4, alpha: 1.0)
        case .white:
            return .white
        case .black:
            return .black
        }
    }
}
