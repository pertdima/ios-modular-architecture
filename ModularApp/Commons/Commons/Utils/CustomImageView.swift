//
//  CustomImageView.swift
//  Commons
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

public class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    public func loadImageUsingUrlString(_ urlString: String, defaultImg: UIImage, completion: @escaping (Bool) -> ()) {
        imageUrlString = urlString
        
        let url = URL(string: urlString)
        image = defaultImg
        self.contentMode = .scaleAspectFit
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            self.contentMode = .scaleToFill
            return
        }
        
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, respones, error) in
            
            if let _ = error {
                return
            }
            
            DispatchQueue.main.async(execute: {
                if let data = data, let imageToCache = UIImage(data: data) {
                    
                    if self.imageUrlString == urlString {
                        self.image = imageToCache
                    }
                    
                    imageCache.setObject(imageToCache, forKey: urlString as NSString)
                    
                    completion(true)
                }else{
                    completion(false)
                }
            })
            
        }).resume()
    }
}
