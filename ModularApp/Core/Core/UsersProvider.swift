//
//  UsersProvider.swift
//  Core
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

public protocol UsersProvider {
    func getUsers()
    func getUser(with id: Int)
}

public struct CCUsersProvider: UsersProvider {
    public func getUsers() {
        //Implement the functionality
    }
    public func getUser(with id: Int) {
        //Implement the functionality
    }
}
