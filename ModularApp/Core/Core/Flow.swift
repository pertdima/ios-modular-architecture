//
//  Flow.swift
//  Core
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

public protocol Flow {
    init(services: Services, navigationVC: UINavigationController?)
    func start()
    var services: Services { get }
    var finish: (_ flow: Flow) -> () { get set }
    var navigation: UINavigationController? { get set }
    var currentVC: UIViewController?  { get }
}

extension Flow {
    public var currentVC: UIViewController? {
        return navigation?.viewControllers.last
    }
}
