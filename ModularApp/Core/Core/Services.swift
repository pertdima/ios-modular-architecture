//
//  Services.swift
//  Core
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

public protocol Services {
    var projectsProvider: ProductsProvider! { get }
    var usersProvider: UsersProvider! { get }
}

public struct CCServices: Services {
    public var projectsProvider: ProductsProvider!
    public var usersProvider: UsersProvider!
    
    public init() {
        self.projectsProvider = CCProductsProvider()
        self.usersProvider = CCUsersProvider()
    }
}
