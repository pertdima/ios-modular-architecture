//
//  CCBaseVC.swift
//  Core
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

open class CCBaseVC: UIViewController {
    
    var services: Services!
    
    public convenience init(services: Services) {
        self.init(nibName: nil, bundle: nil)
        self.services = services
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        print("Did load \(self.classForCoder)")
    }
    
    deinit {
        print("Did deinit \(self.classForCoder)")
    }
    
}
