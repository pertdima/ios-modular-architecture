//
//  ProductsProvider.swift
//  Core
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

public protocol ProductsProvider {
    func getProducts()
    func getProduct(with id: Int)
}

public struct CCProductsProvider: ProductsProvider {
    public func getProducts() {
        //Implement the functionality
    }
    
    public func getProduct(with id: Int) {
        //Implement the functionality
    }
}
