//
//  OauthDelegate.swift
//  OauthModule
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

protocol OauthDelegate: class {
    func wantsToClose(at vc: OauthLoginViewController)
}

