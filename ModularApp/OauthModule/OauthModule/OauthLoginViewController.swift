//
//  OauthLoginViewController.swift
//  OauthModule
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation
import Core
import Commons

class OauthLoginViewController : CCBaseVC {
    weak var delegate: OauthDelegate?
    let textLogin = addComponent.label(id: "text_login", type: .roboto_bold, text: "Login View Controller", size: 20, addColor: .colorPrimary, align: .center)

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addBackgroundColor(addColor: .white)
        constraintItem()
        let rightBarButton = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(self.didClickClose))
        self.navigationItem.title = "Login"
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func loadView() {
        self.view = UIView()
    }
    
    private func constraintItem() {
        view.addSubview(textLogin)
        textLogin.consXCenter(toView: view)
        textLogin.consYCenter(toView: view)
        textLogin.consWidthView(value: 200)
    }
    
    @objc func didClickClose() {
        delegate?.wantsToClose(at: self)
    }
}
