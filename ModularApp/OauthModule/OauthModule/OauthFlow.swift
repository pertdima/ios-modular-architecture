//
//  OauthFlow.swift
//  OauthModule
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation
import Core

public class OauthFlow: Flow, OauthDelegate {
    
    public var finish: (Flow) -> () = { _ in }
    public var services: Services
    public var navigation: UINavigationController?
    
    public required init(services: Services, navigationVC: UINavigationController?) {
        self.services = services
        self.navigation = navigationVC
    }
    
    public func start() {
        presentLoginOauthViewController()
    }
    
    private func presentLoginOauthViewController() {
        let vc = OauthLoginViewController(services: self.services)
        vc.delegate = self
        let vcNav = UINavigationController(rootViewController: vc)
        self.navigation?.present(vcNav, animated: true, completion: nil)
    }
    
    func wantsToClose(at vc: OauthLoginViewController) {
        vc.dismiss(animated: true, completion: nil)
        finish(self)
    }
}

