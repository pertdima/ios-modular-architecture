//
//  ViewController.swift
//  ModularApp
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import UIKit
import Commons
import Core
import RxSwift
import RxCocoa

class HomeViewController: CCBaseVC {
    let disposeBag = DisposeBag()
    weak var delegate: MainAppDelegate?
    let buttonOauthLogin = addComponent.button(id: "ouath_login", title: "Oauth", corner: 0, bgColor: .colorPrimary, textColor: .white, isBorder: true)
    
    let buttonAcademy =  addComponent.button(id: "academy_module", title: "Academy", corner: 0, bgColor: .colorPrimary, textColor: .white, isBorder: true)
    
    let buttonRetropekt =  addComponent.button(id: "retrospekt_module", title: "Retrospekt", corner: 0, bgColor: .colorPrimary, textColor: .white, isBorder: true)
    
    let buttonCareerHub =  addComponent.button(id: "ch_module", title: "Career Hub", corner: 0, bgColor: .colorPrimary, textColor: .white, isBorder: true)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addBackgroundColor(addColor: .white)
        setConstraintItem()
        onClickItem()
    }
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.main.bounds)
    }
    
    private func setConstraintItem() {
        view.addSubview(buttonOauthLogin)
        view.addSubview(buttonAcademy)
        view.addSubview(buttonRetropekt)
        view.addSubview(buttonCareerHub)
        
        buttonOauthLogin.consTopSuperView(view: view, value: 24)
        buttonOauthLogin.consLeftSuperView(view: view, value: 24)
        buttonOauthLogin.consRightSuperView(view: view, value: 24)
        buttonOauthLogin.consHeightView(value: 60)
        
        buttonAcademy.consTopToView(toView: buttonOauthLogin, value: 24)
        buttonAcademy.consLeftSuperView(view: view, value: 24)
        buttonAcademy.consRightSuperView(view: view, value: 24)
        buttonAcademy.consHeightView(value: 60)
        
        buttonRetropekt.consTopToView(toView: buttonAcademy, value: 24)
        buttonRetropekt.consLeftSuperView(view: view, value: 24)
        buttonRetropekt.consRightSuperView(view: view, value: 24)
        buttonRetropekt.consHeightView(value: 60)
        
        buttonCareerHub.consTopToView(toView: buttonRetropekt, value: 24)
        buttonCareerHub.consLeftSuperView(view: view, value: 24)
        buttonCareerHub.consRightSuperView(view: view, value: 24)
        buttonCareerHub.consHeightView(value: 60)
    }
    
    private func onClickItem() {
        buttonOauthLogin.rx.tap.subscribe(onNext : {
            self.delegate?.presentLoginOauth()
        }).disposed(by: disposeBag)
        
        buttonAcademy.rx.tap.subscribe(onNext : {
            self.delegate?.pushAcademy()
        }).disposed(by: disposeBag)
    }
}

