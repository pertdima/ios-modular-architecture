//
//  MainAppDelegate.swift
//  ModularApp
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation

protocol MainAppDelegate : class {
    func presentLoginOauth()
    func pushAcademy()
}
