//
//  AppFlow.swift
//  ModularApp
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import Foundation
import Core
import OauthModule
import AcademyModule

class AppFlow : Flow {
    var services: Services
    var childFlows: [Flow] = [Flow]()
    var navigation: UINavigationController?
    var finish: (Flow) -> () = { _ in }
    
    required init(services: Services, navigationVC: UINavigationController? = nil) {
        self.services = services
    }
    
    public func start() {
        self.presentMainVC()
    }
    
    private func presentMainVC() {
        let mainVC = HomeViewController(services: self.services)
        mainVC.delegate = self
        self.navigation = UINavigationController(rootViewController: mainVC)
    }
}

extension AppFlow : MainAppDelegate {
    func presentLoginOauth() {
        let oauthFlow = OauthFlow(services: self.services, navigationVC: self.navigation)
        oauthFlow.finish = { flow in
            _ = self.childFlows.popLast()
            print("poped Login)")
        }
        startNew(flow: oauthFlow)
    }
    
    func pushAcademy() {
        let academyFlow = AcademyFlow(services: self.services, navigationVC: self.navigation)
        academyFlow.finish = { flow in
            _ = self.childFlows.popLast()
            print("poped academy)")
        }
        startNew(flow: academyFlow)
    }
    
    
    private func startNew(flow: Flow) {
        flow.start()
        self.childFlows.append(flow)
    }
}
