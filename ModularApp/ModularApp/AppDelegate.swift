//
//  AppDelegate.swift
//  ModularApp
//
//  Created by Engineer on 14/02/19.
//  Copyright © 2019 Engineer. All rights reserved.
//

import UIKit
import Core

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var flow: AppFlow!
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        initFlowAndServices()
        return true
    }
    
    private func initFlowAndServices() {
        let services = CCServices()
        self.flow = AppFlow(services: services)
        flow.start()
        self.window = UIWindow()
        self.window?.rootViewController = flow.navigation
        self.window?.makeKeyAndVisible()
    }
    
}

